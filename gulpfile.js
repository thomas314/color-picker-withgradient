let gulp = require('gulp');
let pcss = require('gulp-postcss');
let rename = require('gulp-rename');
let cleanCSS = require('gulp-clean-css');
let uglify = require("gulp-uglify");
var pipeline = require('readable-stream').pipeline;

// Pour uglify 
gulp.task('compress', () => {
    return pipeline(
          gulp.src('js/app.js'),
          uglify(),
          gulp.dest('dist')
    );
  });


// Pour minify
gulp.task('minify-css', () => {
  return gulp.src('styles/*.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('dist'));
});


gulp.task('minify-css', () => {
    return gulp.src('css/style.css')
    .pipe(cleanCSS({debug: true}, (details) => {
        console.log(`${details.name}: ${details.stats.originalSize}`);
        console.log(`${details.name}: ${details.stats.minifiedSize}`);
    }))
    .pipe(gulp.dest('dist'));
});


// tests postcss, autoprefixer et rucksack-css
var processors = [
    require('rucksack-css'),
    require('autoprefixer')({ browsers:['last 2 versions']})
    // require('cssnano')()

]
gulp.task('css', () => {
    gulp.src('css/style.css')
        .pipe(pcss([processors]))
        .pipe()
        .pipe(gulp.dest('css/'))
    })